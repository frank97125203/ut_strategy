﻿namespace replace_condition_logic_with_strategy
{
    public class Product
    {
        public Product(double length, double width, double height, double weight)
        {
            Length = length;
            Width = width;
            Height = height;
            Weight = weight;
        }
        public double Length { get; }
        public double Width { get; }
        public double Height { get; }
        public double Weight { get; }
        public double Size() => Length * Width * Height;
    }
}