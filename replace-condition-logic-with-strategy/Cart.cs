﻿#region

using System;
using System.Collections.Generic;

#endregion

namespace replace_condition_logic_with_strategy
{
    public class Cart
    {
        private readonly Dictionary<string, Func<Product, double>> _shippingFeeFormulas = new Dictionary<string, Func<Product, double>>
            {
                { "black cat", CalculateFeeByBlackcat },
                { "hsinchu", CalculateFeeByHsinchu },
                { "post office", CalculateFeeByPostOffice }
            };
        public double shippingFee(string shipper, Product product)
        {
            return _shippingFeeFormulas.ContainsKey(shipper) ? 
                _shippingFeeFormulas[shipper](product) : 
                throw new ArgumentException("shipper not exist");
            //switch (shipper)
            //{
            //    case "black cat":
            //        return CalculateFeeByBlackcat(product);
            //    case "hsinchu":
            //        return CalculateFeeByHsinchu(product);
            //    case "post office":
            //        return CalculateFeeByPostOffice(product);
            //    default:
            //        throw new ArgumentException("shipper not exist");
            //}
            //if (shipper.Equals("black cat"))
            //{
            //    return CalculateFeeByBlackcat(product);
            //}
            //else if (shipper.Equals("hsinchu"))
            //{
            //    return CalculateFeeByHsinchu(product);
            //}
            //else if (shipper.Equals("post office"))
            //{
            //    return CalculateFeeByPostoffice(product);
            //}
            //else
            //{
            //    throw new ArgumentException("shipper not exist");
            //}
        }

        private static double CalculateFeeByPostOffice(Product product)
        {
            var feeByWeight = 80 + product.Weight * 10;
            var feeBySize = product.Size() * 0.00002 * 1100;
            return Math.Min(feeByWeight, feeBySize);
        }

        private static double CalculateFeeByHsinchu(Product product)
        {
            if (product.Length > 100 || product.Width > 100 || product.Height > 100)
            {
                return product.Size() * 0.00002 * 1100 + 500;
            }
            else
            {
                return product.Size() * 0.00002 * 1200;
            }
        }

        private static double CalculateFeeByBlackcat(Product product)
        {
            if (product.Weight > 20)
            {
                return 500;
            }
            else
            {
                return 100 + product.Weight * 10;
            }
        }
    }
}